import React, { useEffect } from "react";
import type { AppProps /*, AppContext */ } from "next/app";
import Head from "next/head";

import "../styles/App.css";
import Sidebar from "../components/Layout/Sider";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Sidebar>
        <Component {...pageProps} />
      </Sidebar>
    </>
  );
}

export default MyApp;
