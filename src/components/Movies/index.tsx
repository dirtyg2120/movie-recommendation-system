import { Row, Col } from "antd"
import React from "react"
import styles from '../../styles/Home.module.css'
import { Movie } from "../../models/movie"

const thumbnail = {
  height: '430px'
}

const Movies =  (MoviesList: Array<Movie>) => {
  return (
  <>
  <div style={{ minHeight: '50px' }} />
  <Row gutter={[16, 24]}>
    {
      MoviesList.map((movie: any, i: number) =>
        <Col className="gutter-row" span={6} key={i}>
          <div style={thumbnail}>
          <img src={movie.thumbnail} width='100%'/>
          </div>
          <div className={styles.movieTitle} style={{fontSize:'15px', fontWeight:'bold'}}>{movie.title}</div>
        </Col>)
    }
  </Row>
  </>
  )
}

export default Movies