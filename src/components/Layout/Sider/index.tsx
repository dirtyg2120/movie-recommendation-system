import { Layout, Menu, Breadcrumb, Col, Row } from 'antd';
import {
  HeartFilled,
  FireFilled,
  SearchOutlined
} from '@ant-design/icons';
import React, { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react';
import styles from '../../../styles/Home.module.css'
import Movies from '../../Movies';

const { Content, Sider } = Layout;


interface SidebarProps {
  children: any;
}

const webName = {
  color: 'white',
  padding: '10px 20px 0px 0px',
  fontSize: '13px',
}

const FavouriteList = [
  {
    thumbnail: 'https://m.media-amazon.com/images/M/MV5BZWFlYmY2MGEtZjVkYS00YzU4LTg0YjQtYzY1ZGE3NTA5NGQxXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_FMjpg_UX1000_.jpg',
    title: 'The Shining'
  },
  {
    thumbnail: 'https://m.media-amazon.com/images/M/MV5BMjlmZmI5MDctNDE2YS00YWE0LWE5ZWItZDBhYWQ0NTcxNWRhXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_FMjpg_UX1000_.jpg',
    title: 'Spirited Away'
  },
  {
    thumbnail: 'https://m.media-amazon.com/images/M/MV5BMzkzMmU0YTYtOWM3My00YzBmLWI0YzctOGYyNTkwMWE5MTJkXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_FMjpg_UX1000_.jpg',
    title: 'Braveheart'
  },
  {
    thumbnail: 'https://m.media-amazon.com/images/M/MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_FMjpg_UX1000_.jpg',
    title: 'One Flew Over the Cuckoo\'s Nest'
  },
  {
    thumbnail: 'https://m.media-amazon.com/images/M/MV5BNjViNWRjYWEtZTI0NC00N2E3LTk0NGQtMjY4NTM3OGNkZjY0XkEyXkFqcGdeQXVyMjUxMTY3ODM@._V1_FMjpg_UX1000_.jpg',
    title: 'Hamilton'
  },
]

const Favour

function switchContent(key: any) {
  switch (key) {
    case '1':
      return Movies(RecommendList)
    case '2':
      return Movies(FavouriteList)
    case '3':
      return   
  }
}

const Sidebar = ({ children }: SidebarProps) => {
  let firstMounted = useRef(false);

  useEffect(() => {
    firstMounted.current = true;
  }, []);
  
  const [selectedMenuItem, setSelectedMenuItem]= useState('1');


  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider trigger={null}>
        <div className={styles.logo}>
          <img src="popcorn.png" width="70" height="70" />
          <p style={webName}>NETFLIX & CHILL</p>
        </div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" selectedKeys={[selectedMenuItem]}
        onClick={(e)=>{setSelectedMenuItem(e.key)}}>
          <Menu.Item key={'1'} icon={<FireFilled />}>
            Recommended
          </Menu.Item>
          <Menu.Item key={'2'} icon={<HeartFilled />}>
            Favorite
          </Menu.Item>
          <Menu.Item key={'3'} icon={<SearchOutlined />}>
            Search
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: '0 16px' }}>
          {switchContent(selectedMenuItem)}
        </Content>
      </Layout>
    </Layout>
  );
}

export default Sidebar